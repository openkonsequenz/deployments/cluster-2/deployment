apiVersion: v1
kind: ServiceAccount
metadata:
  namespace: traefik
  name: traefik-ingress-controller

---
kind: Deployment
apiVersion: apps/v1
metadata:
  namespace: traefik
  name: traefik
  labels:
    app: traefik

spec:
  replicas: 1
  revisionHistoryLimit: 2
  selector:
    matchLabels:
      app: traefik
  template:
    metadata:
      labels:
        app: traefik
    spec:
      serviceAccountName: traefik-ingress-controller
      containers:
        - name: traefik
          image: traefik:v2.5.3
          args:
            - --api.insecure
            - --accesslog
            - --entrypoints.web.address=:80
            - --entrypoints.websecure.address=:443
            - --providers.kubernetescrd
            - --certificatesresolvers.ourcertresolver.acme.tlschallenge
            - --certificatesresolvers.ourcertresolver.acme.email=maintainer@openkonsequenz.de
            - --certificatesresolvers.ourcertresolver.acme.storage=/acme/acme.json
            - --certificatesresolvers.ourcertresolver.acme.httpchallenge.entrypoint=web
            - --providers.kubernetescrd.allowCrossNamespace=true
            # Please note that this is the staging Let's Encrypt server.
            # Once you get things working, you should remove that whole line altogether.
            #- --certificatesresolvers.ourcertresolver.acme.caserver=https://acme-staging-v02.api.letsencrypt.org/directory
            #- --entrypoints.websecure.http.middlewares=traefik-traefik-forward-auth
            #- --entryPoints.web.http.redirections.entryPoint.to=websecure
            #- --entryPoints.web.http.redirections.entryPoint.scheme=https
          ports:
            - name: web
              containerPort: 80
            - name: websecure
              containerPort: 443
            - name: admin
              containerPort: 8080
          volumeMounts:
          - mountPath: /acme
            name: acme
      volumes:
      - name: acme
        persistentVolumeClaim:
          claimName: traefik-acme

---
apiVersion: v1
kind: Service
metadata:
  namespace: traefik
  name: traefik
  labels:
    app: traefik
  annotations:
    service.beta.kubernetes.io/azure-load-balancer-resource-group: {{ .Values.clusterAzureNamespace }}
spec:
  type: LoadBalancer
  loadBalancerIP: {{ .Values.clusterIP }}
  ports:
    - protocol: TCP
      name: web
      port: 80
      targetPort: 80
    - protocol: TCP
      name: websecure
      port: 443
      targetPort: 443
  selector:
    app: traefik