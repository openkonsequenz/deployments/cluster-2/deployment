#  *******************************************************************************
#  Copyright (c) 2019 Contributors to the Eclipse Foundation
#
#  See the NOTICE file(s) distributed with this work for additional
#  information regarding copyright ownership.
#
#  This program and the accompanying materials are made available under the
#  terms of the Eclipse Public License v. 2.0 which is available at
#  http://www.eclipse.org/legal/epl-2.0.
#
#  SPDX-License-Identifier: EPL-2.0
#  *******************************************************************************
backend-version: 1.0.0
spring:
  datasource:
    url: ${DATASOURCE_URL}
    username: ${APP_DB_ROLE}
    password: ${APP_DB_PASSWORD}
   # url: jdbc:postgresql://entopticadirx:5432/GridFailureInfoDevServer
   # username: ${GFI_DB_USERNAME}
   # password: ${GFI_DB_PASSWORD}
  #  max-active: 20
  flyway:
    enabled: false

  # RabbitMQ configuration
  rabbitmq:
    host: rabbitmq-svc.{{ .Values.sharedEnvironment }}.svc.cluster.local
    port: 5672
    username: guest
    password: guest

    # Importchannel
    importExchange: sitImportExchange
    importQueue: sitImportQueue
    importkey: sitImportExchange.failureImportKey

    # Exportchannels
    exportExchange: sitExportExchange
    channels:
      - name: Mail
        exportQueue: sit_mail_export_queue
        exportKey: sit_mail_export_key
        isMailType: true
      - name: Störungsauskunft.de
        exportQueue: sit_stoerungsauskunft_export_queue
        exportKey: sit_stoerungsauskunft_export_key
      - name: Störinfotool-eigene-Web-Komponenten
        exportQueue: sit_own_export_queue
        exportKey: sit_own_export_key

  # UI setting (Map)
  settings:
    overviewMapInitialZoom: 10
    detailMapInitialZoom: 10
    overviewMapInitialLatitude: 49.850193
    overviewMapInitialLongitude: 8.366089
    daysInPastToShowClosedInfos: 365
    dataExternInitialVisibility: show

    isUseHtmlEmailBtnTemplate: true
    # Mail settings (Default templates)
    emailSubjectPublishInit: "Die Störung (Sparte: $Sparte$) mit Beginn: $Störungsbeginn_gemeldet$ wurde in den Status veröffentlicht geändert."
    emailContentPublishInit: "<p><b>Veröffentlicht [TEST]</b></p>
                              <p>Sehr geehrte Damen und Herren,</p>
                              <p>die im Betreff genannte Meldung ist über folgenden Link erreichbar:</p>
                              $Direkter_Link_zur_Störung$
                              <p>Mit freundlichen Grüßen</p>
                              <p>Ihr Admin-Meister-Team der PTA GmbH</p>"
    emailSubjectUpdateInit: "Die Störung (Sparte: $Sparte$) mit Beginn: $Störungsbeginn_gemeldet$ wurde in den Status aktualisiert geändert."
    emailContentUpdateInit: "<p><b>Aktualisiert [TEST]</b></p>
                             <p>Sehr geehrte Damen und Herren,</p>
                             <p>die im Betreff genannte Meldung ist über folgenden Link erreichbar:</p>
                             $Direkter_Link_zur_Störung$
                             <p>Mit freundlichen Grüßen</p>
                             <p>Ihr Admin-Meister-Team der PTA GmbH</p>"
    emailSubjectCompleteInit: "Die Störung (Sparte: $Sparte$) mit Beginn: $Störungsbeginn_gemeldet$ wurde in den Status beendet geändert."
    emailContentCompleteInit: "<p><b>Beendet [TEST]</b></p>
                              <p>Sehr geehrte Damen und Herren,</p>
                              <p>die im Betreff genannte Meldung ist über folgenden Link erreichbar:</p>
                              $Direkter_Link_zur_Störung$
                              <p>Mit freundlichen Grüßen</p>
                              <p>Ihr Admin-Meister-Team der PTA GmbH</p>"

    visibilityConfiguration:
      fieldVisibility:
        failureClassification: show
        responsibility: show
        description: show
        internalRemark: show
      tableInternColumnVisibility:
        failureClassification: show
        responsibility: show
        description: show
        statusIntern: show
        statusExtern: show
        publicationStatus: show
        branch: show
        voltageLevel: show
        pressureLevel: show
        failureBegin: show
        failureEndPlanned: show
        failureEndResupplied: show
        expectedReasonText: show
        internalRemark: show
        postcode: show
        city: show
        district: show
        street: show
        housenumber: show
        radius: show
        stationIds: show
      tableExternColumnVisibility:
        branch: show
        failureBegin: show
        failureEndPlanned: show
        expectedReasonText: show
        description: show
        postcode: show
        city: show
        district: show
        street: show
        failureClassification: show
      mapExternTooltipVisibility:
        failureBegin: show
        failureEndPlanned: show
        expectedReasonText: show
        branch: show
        postcode: show
        city: show
        district: show
  jpa:
    show-sql: false

logging:
  level:
    root: INFO
    org.eclipse.openk: INFO
    org.springframework.web: ERROR
    org.hibernate: ERROR

server:
  port: 9165
  max-http-header-size: 262144

jwt:
  tokenHeader: Authorization
  useStaticJwt: false
  staticJwt: x

gridFailureInformation:
  maxListSize: 2000

# Services configuration
services:
  authNAuth:
    name: authNAuthService
    technical-username: admin
    technical-userpassword: admin
  contacts:
    name: contactService
    communicationType:
      mobile: Mobil
    useModuleNameForFilter: false
    moduleName: Störungsinformationstool
  sitCache:
    name: sitCacheService

portalFeLoginURL: http://portal-ok.de/portalFE/#/login
portalFeModulename: SIT DEV
authNAuthService:
  ribbon:
    listOfServers: http://portal-service:80

contactService:
  ribbon:
    listOfServers: http://contact-be-service:9155

sitCacheService:
  ribbon:
    listOfServers: http://gfi-web-cache-service:3003

feign:
  client:
    config:
      default:
        connectTimeout: 60000
        readTimeout: 60000
cors:
  corsEnabled: false

process:
  definitions:
    classification:
      plannedMeasureDbid: 2

reminder:
  status-change:
    minutes-before: 1440

distribution-group-publisher:
  name: Veröffentlicher
  distribution-text: Bitte anpassen

export-to-dmz:
  enabled: true
  cron: 0 */1 * ? * *

swagger:
  enabled: true

security:
  ignored: /swagger-resources/**
